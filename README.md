
# go-nowish

Simply Go libary with useful time utilies:

- Clock: blazing-fast clock giving good (ish) representations of the now time.
- Timeout: a reusable timeout structure useful for extending timeouts.
